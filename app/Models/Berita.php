<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    use HasFactory;
    protected $fillable = [
        'kategori_id','utama','gambar','judul', 'isi','admin_id','open','created_at','updated_at'
    ];
    public function KategoriId(){
        return $this->belongsTo('App\Models\KategoriBerita','kategori_id');
    }
    public function UserId(){
        return $this->belongsTo('App\Models\User','admin_id');
    }
}
