<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Berita;
use App\Models\Kontak;

class UmumController extends Controller
{
    public function index(){
        $utamas = Berita::where('utama','yes')->first();
        return view('index',compact('utamas'));
    }
    public function tentang(){
        return view('tentang');
    }
    public function berita(){
        return view('berita');
    }
    public function detail_berita($judul){
        $datas = Berita::where('judul',str_replace('_',' ',$judul))->first();
        if (!$datas) {
            return redirect()->back();
        }
        return view('detail_berita', compact('datas'));
    }
    public function kontak(){
        $datas = Kontak::first();
        return view('kontak',compact('datas'));
    }
    public function video(){
        return view('video');
    }
    public function pengurus(){
        return view('pengurus');
    }
    public function struktur_organisasi(){
        return view('struktur_organisasi');
    }
}
