<div class="navbar-area">
            <div class="main-responsive-nav">
                <div class="container">
                    <div class="main-responsive-menu">
                        <div class="logo">
                            <a href="index.html">
                                <img src="{{asset('assets/img/tci-hitam.png')}}" alt="image">
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="main-navbar">
                <div class="container">
                    <nav class="navbar navbar-expand-md navbar-light">
                        <a class="navbar-brand" href="index.html">
                            <img src="{{asset('assets/img/tci-hitam.png')}}" alt="image">
                        </a>

                        <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a href="{{ route('index') }}" class="nav-link {{ setActive('index') }}">
                                        Beranda
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('tentang') }}" class="nav-link {{ setActive('tentang') }}">
                                        Tentang
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('berita') }}" class="nav-link {{ setActive('berita') }}">
                                        Berita
                                    </a>
                                </li>
                                

                                <li class="nav-item">
                                    <a href="{{ route('video') }}" class="nav-link {{ setActive('video') }}">
                                        Video
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('pengurus') }}" class="nav-link {{ setActive('pengurus') }}">
                                        Pengurus
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('struktur-organisasi') }}" class="nav-link {{ setActive('struktur-organisasi') }}">
                                        Struktuk Organisasi
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('kontak') }}" class="nav-link {{ setActive('kontak') }}">
                                        Kontak
                                    </a>
                                </li>
                            </ul>

                            <div class="others-options d-flex align-items-center">
                                <div class="option-item">
                                    <form class="search-box">
                                        <input type="text" class="form-control" placeholder="Search for..">
                                        <button type="submit"><i class='bx bx-search'></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>

            <div class="others-option-for-responsive">
                <div class="container">
                    <div class="dot-menu">
                        <div class="inner">
                            <div class="circle circle-one"></div>
                            <div class="circle circle-two"></div>
                            <div class="circle circle-three"></div>
                        </div>
                    </div>
                    
                    <div class="container">
                        <div class="option-inner">
                            <div class="others-options d-flex align-items-center">
                                <div class="option-item">
                                    <form class="search-box">
                                        <input type="text" class="form-control" placeholder="Search for..">
                                        <button type="submit"><i class='bx bx-search'></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>