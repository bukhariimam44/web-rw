@extends('layouts.umum')
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
            <div class="container">
                <div class="page-title-content">
                    <h2>About us</h2>
                    <ul>
                        <li><a href="index.html">Home</a></li>
                        <li>About us</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Page Banner -->

        <!-- Start About Area -->
        <section class="about-area ptb-50">
            <div class="container">
                <div class="about-image">
                    <img src="assets/img/about.jpg" alt="image">
                </div>

                <div class="about-content">
                    <h3>Organizing conference among ourselves to make it better financially</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.</p>

                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                </div>
            </div>
        </section>
        <!-- End About Area -->
@endsection