@extends('layouts.umum')
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
            <div class="container">
                <div class="page-title-content">
                    <h2>Video</h2>
                    <ul>
                        <li><a href="index.html">Home</a></li>
                        <li>Video</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Page Banner -->
        
        <!-- Start Default News Area -->
        <section class="default-news-area ptb-50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="single-video-item">
                                    <div class="video-news-image">
                                        <a href="#">
                                            <img src="assets/img/video-news/video-news-5.jpg" alt="image">
                                        </a>
    
                                        <a href="https://www.youtube.com/watch?v=UG8N5JT4QLc" class="popup-youtube">
                                            <i class='bx bx-play-circle'></i>
                                        </a>
                                    </div>
    
                                    <div class="video-news-content">
                                        <span>Sports</span>
                                        <h3>
                                            <a href="#">Manchester United’s dream of winning by a goal was fulfilled</a>
                                        </h3>
                                        <p><a href="#">Patricia</a> / 28 September, 2021</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="single-video-item">
                                    <div class="video-news-image">
                                        <a href="#">
                                            <img src="assets/img/video-news/video-news-6.jpg" alt="image">
                                        </a>
    
                                        <a href="https://www.youtube.com/watch?v=UG8N5JT4QLc" class="popup-youtube">
                                            <i class='bx bx-play-circle'></i>
                                        </a>
                                    </div>
    
                                    <div class="video-news-content">
                                        <span>Fitness</span>
                                        <h3>
                                            <a href="#">Morning yoga is very important for maintaining good physical fitness</a>
                                        </h3>
                                        <p><a href="#">Patricia</a> / 28 September, 2021</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="single-video-item">
                                    <div class="video-news-image">
                                        <a href="#">
                                            <img src="assets/img/video-news/video-news-7.jpg" alt="image">
                                        </a>
    
                                        <a href="https://www.youtube.com/watch?v=UG8N5JT4QLc" class="popup-youtube">
                                            <i class='bx bx-play-circle'></i>
                                        </a>
                                    </div>
    
                                    <div class="video-news-content">
                                        <span>Culture</span>
                                        <h3>
                                            <a href="#">Tourist centers of different countries have once againg become popular</a>
                                        </h3>
                                        <p><a href="#">Patricia</a> / 28 September, 2021</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="single-video-item">
                                    <div class="video-news-image">
                                        <a href="#">
                                            <img src="assets/img/video-news/video-news-8.jpg" alt="image">
                                        </a>
    
                                        <a href="https://www.youtube.com/watch?v=UG8N5JT4QLc" class="popup-youtube">
                                            <i class='bx bx-play-circle'></i>
                                        </a>
                                    </div>
    
                                    <div class="video-news-content">
                                        <span>Culture</span>
                                        <h3>
                                            <a href="#">After a long period the people of different countries have become vocal again.</a>
                                        </h3>
                                        <p><a href="#">Patricia</a> / 28 September, 2021</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="video-item mb-30">
                                    <div class="video-news-image">
                                        <a href="#">
                                            <img src="assets/img/video-news/video-news-1.jpg" alt="image">
                                        </a>
    
                                        <a href="https://www.youtube.com/watch?v=UG8N5JT4QLc" class="popup-youtube">
                                            <i class='bx bx-play-circle'></i>
                                        </a>
                                    </div>
    
                                    <div class="video-news-content">
                                        <h3>
                                            <a href="#">Apply these 10 secret techniques to improve travel</a>
                                        </h3>
                                        <span>28  September, 2021</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="video-item mb-30">
                                    <div class="video-news-image">
                                        <a href="#">
                                            <img src="assets/img/video-news/video-news-2.jpg" alt="image">
                                        </a>
    
                                        <a href="https://www.youtube.com/watch?v=UG8N5JT4QLc" class="popup-youtube">
                                            <i class='bx bx-play-circle'></i>
                                        </a>
                                    </div>
    
                                    <div class="video-news-content">
                                        <h3>
                                            <a href="#">The lazy man’s guide to travel you to our moms</a>
                                        </h3>
                                        <span>28  September, 2021</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="video-item mb-30">
                                    <div class="video-news-image">
                                        <a href="#">
                                            <img src="assets/img/video-news/video-news-3.jpg" alt="image">
                                        </a>
    
                                        <a href="https://www.youtube.com/watch?v=UG8N5JT4QLc" class="popup-youtube">
                                            <i class='bx bx-play-circle'></i>
                                        </a>
                                    </div>
    
                                    <div class="video-news-content">
                                        <h3>
                                            <a href="#">A beautiful feeling Franch winning the final match</a>
                                        </h3>
                                        <span>28  September, 2021</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="video-item mb-30">
                                    <div class="video-news-image">
                                        <a href="#">
                                            <img src="assets/img/video-news/video-news-4.jpg" alt="image">
                                        </a>
    
                                        <a href="https://www.youtube.com/watch?v=UG8N5JT4QLc" class="popup-youtube">
                                            <i class='bx bx-play-circle'></i>
                                        </a>
                                    </div>
    
                                    <div class="video-news-content">
                                        <h3>
                                            <a href="#">This movement aims to establish women rights</a>
                                        </h3>
                                        <span>28  September, 2021</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="single-video-item">
                            <div class="row align-items-center">
                                <div class="col-lg-4">
                                    <div class="video-news-image">
                                        <a href="#">
                                            <img src="assets/img/video-news/video-news-9.jpg" alt="image">
                                        </a>
        
                                        <a href="https://www.youtube.com/watch?v=UG8N5JT4QLc" class="popup-youtube">
                                            <i class='bx bx-play-circle'></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-lg-8">
                                    <div class="video-news-content mt-0">
                                        <span>Politic</span>
                                        <h3>
                                            <a href="#">Movement with 5 major issues keeping the people together</a>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        <p>28 September, 2021</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="single-video-item">
                            <div class="row align-items-center">
                                <div class="col-lg-4">
                                    <div class="video-news-image">
                                        <a href="#">
                                            <img src="assets/img/video-news/video-news-10.jpg" alt="image">
                                        </a>
        
                                        <a href="https://www.youtube.com/watch?v=UG8N5JT4QLc" class="popup-youtube">
                                            <i class='bx bx-play-circle'></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-lg-8">
                                    <div class="video-news-content mt-0">
                                        <span>Culture</span>
                                        <h3>
                                            <a href="#">This year, as every year, the lunar festival is celebrated in Philadelphia Chinatown</a>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        <p>28 September, 2021</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="single-video-item">
                            <div class="row align-items-center">
                                <div class="col-lg-4">
                                    <div class="video-news-image">
                                        <a href="#">
                                            <img src="assets/img/video-news/video-news-11.jpg" alt="image">
                                        </a>
        
                                        <a href="https://www.youtube.com/watch?v=UG8N5JT4QLc" class="popup-youtube">
                                            <i class='bx bx-play-circle'></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-lg-8">
                                    <div class="video-news-content mt-0">
                                        <span>Sport</span>
                                        <h3>
                                            <a href="#">Start a new men’s road World Championships US Sport</a>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        <p>28 September, 2021</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="single-video-item">
                            <div class="row align-items-center">
                                <div class="col-lg-4">
                                    <div class="video-news-image">
                                        <a href="#">
                                            <img src="assets/img/video-news/video-news-12.jpg" alt="image">
                                        </a>
        
                                        <a href="https://www.youtube.com/watch?v=UG8N5JT4QLc" class="popup-youtube">
                                            <i class='bx bx-play-circle'></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-lg-8">
                                    <div class="video-news-content mt-0">
                                        <span>Technology</span>
                                        <h3>
                                            <a href="#">Like humans, the new robot has a lot of memory power.</a>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        <p>28 September, 2021</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="single-video-item">
                            <div class="row align-items-center">
                                <div class="col-lg-4">
                                    <div class="video-news-image">
                                        <a href="#">
                                            <img src="assets/img/video-news/video-news-13.jpg" alt="image">
                                        </a>
        
                                        <a href="https://www.youtube.com/watch?v=UG8N5JT4QLc" class="popup-youtube">
                                            <i class='bx bx-play-circle'></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-lg-8">
                                    <div class="video-news-content mt-0">
                                        <span>Covid-19</span>
                                        <h3>
                                            <a href="#">The Covid-19 vaccine is being given as a test</a>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        <p>28 September, 2021</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="single-video-item">
                            <div class="row align-items-center">
                                <div class="col-lg-4">
                                    <div class="video-news-image">
                                        <a href="#">
                                            <img src="assets/img/video-news/video-news-14.jpg" alt="image">
                                        </a>
        
                                        <a href="https://www.youtube.com/watch?v=UG8N5JT4QLc" class="popup-youtube">
                                            <i class='bx bx-play-circle'></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-lg-8">
                                    <div class="video-news-content mt-0">
                                        <span>Football</span>
                                        <h3>
                                            <a href="#">The last time of the match is goning on</a>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                        <p>28 September, 2021</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="pagination-area">
                            <a href="#" class="prev page-numbers">
                                <i class='bx bx-chevron-left'></i>
                            </a>
                            <a href="#" class="page-numbers">1</a>
                            <span class="page-numbers current" aria-current="page">2</span>
                            <a href="#" class="page-numbers">3</a>
                            <a href="#" class="page-numbers">4</a>
                            <a href="#" class="next page-numbers">
                                <i class='bx bx-chevron-right'></i>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <aside class="widget-area">
                            <section class="widget widget_latest_news_thumb">
                                <h3 class="widget-title">Latest news</h3>

                                <article class="item">
                                    <a href="#" class="thumb">
                                        <span class="fullimage cover bg1" role="img"></span>
                                    </a>
                                    <div class="info">
                                        <h4 class="title usmall"><a href="#">Negotiations on a peace agreement between the two countries</a></h4>
                                        <span>28 September, 2021</span>
                                    </div>
                                </article>

                                <article class="item">
                                    <a href="#" class="thumb">
                                        <span class="fullimage cover bg2" role="img"></span>
                                    </a>
                                    <div class="info">
                                        <h4 class="title usmall"><a href="#">Love songs helped me through heartbreak</a></h4>
                                        <span>28 September, 2021</span>
                                    </div>
                                </article>

                                <article class="item">
                                    <a href="#" class="thumb">
                                        <span class="fullimage cover bg3" role="img"></span>
                                    </a>
                                    <div class="info">
                                        <h4 class="title usmall"><a href="#">This movement aims to establish women rights</a></h4>
                                        <span>28 September, 2021</span>
                                    </div>
                                </article>

                                <article class="item">
                                    <a href="#" class="thumb">
                                        <span class="fullimage cover bg4" role="img"></span>
                                    </a>
                                    <div class="info">
                                        <h4 class="title usmall"><a href="#">Giving special powers to police officers to prevent crime</a></h4>
                                        <span>28 September, 2021</span>
                                    </div>
                                </article>
                            </section>

                            <section class="widget widget_popular_posts_thumb">
                                <h3 class="widget-title">Popular posts</h3>

                                <article class="item">
                                    <a href="#" class="thumb">
                                        <span class="fullimage cover bg1" role="img"></span>
                                    </a>
                                    <div class="info">
                                        <h4 class="title usmall"><a href="#">Match between United States and England at AGD stadium</a></h4>
                                        <span>28 September, 2021</span>
                                    </div>
                                </article>

                                <article class="item">
                                    <a href="#" class="thumb">
                                        <span class="fullimage cover bg2" role="img"></span>
                                    </a>
                                    <div class="info">
                                        <h4 class="title usmall"><a href="#">For the last time, he addressed the people</a></h4>
                                        <span>28 September, 2021</span>
                                    </div>
                                </article>

                                <article class="item">
                                    <a href="#" class="thumb">
                                        <span class="fullimage cover bg3" role="img"></span>
                                    </a>
                                    <div class="info">
                                        <h4 class="title usmall"><a href="#">The coronavairus is finished and the outfit is busy</a></h4>
                                        <span>28 September, 2021</span>
                                    </div>
                                </article>

                                <article class="item">
                                    <a href="#" class="thumb">
                                        <span class="fullimage cover bg4" role="img"></span>
                                    </a>
                                    <div class="info">
                                        <h4 class="title usmall"><a href="#">A fierce battle is going on between the two in the game</a></h4>
                                        <span>28 September, 2021</span>
                                    </div>
                                </article>
                            </section>

                            <section class="widget widget_stay_connected">
                                <h3 class="widget-title">Stay connected</h3>
                                
                                <ul class="stay-connected-list">
                                    <li>
                                        <a href="#">
                                            <i class='bx bxl-facebook'></i>
                                            120,345 Fans
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#" class="twitter">
                                            <i class='bx bxl-twitter'></i>
                                            25,321 Followers
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#" class="linkedin">
                                            <i class='bx bxl-linkedin'></i>
                                            7,519 Connect
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#" class="youtube">
                                            <i class='bx bxl-youtube'></i>
                                            101,545 Subscribers
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#" class="instagram">
                                            <i class='bx bxl-instagram'></i>
                                            10,129 Followers
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#" class="wifi">
                                            <i class='bx bx-wifi'></i>
                                            952 Subscribers
                                        </a>
                                    </li>
                                </ul>
                            </section>

                            <section class="widget widget_featured_reports">
                                <h3 class="widget-title">Featured reports</h3>

                                <div class="single-featured-reports">
                                    <div class="featured-reports-image">
                                        <a href="#">
                                            <img src="assets/img/featured-reports/featured-reports-1.jpg" alt="image">
                                        </a>

                                        <div class="featured-reports-content">
                                            <h3>
                                                <a href="#">All the highlights from western fashion week summer 2021</a>
                                            </h3>
                                            <p><a href="#">Patricia</a> / 28 September, 2021</p>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <section class="widget widget_newsletter">
                                <div class="newsletter-content">
                                    <h3>Subscribe to our newsletter</h3>
                                    <p>Subscribe to our newsletter to get the new updates!</p>
                                </div>   

                                <form class="newsletter-form" data-toggle="validator">
                                    <input type="email" class="input-newsletter" placeholder="Enter your email" name="EMAIL" required autocomplete="off">
            
                                    <button type="submit">Subscribe</button>
                                    <div id="validator-newsletter" class="form-result"></div>
                                </form>
                            </section>

                            <section class="widget widget_most_shared">
                                <h3 class="widget-title">Most shared</h3>

                                <div class="single-most-shared">
                                    <div class="most-shared-image">
                                        <a href="#">
                                            <img src="assets/img/most-shared/most-shared-2.jpg" alt="image">
                                        </a>

                                        <div class="most-shared-content">
                                            <h3>
                                                <a href="#">All the highlights from western fashion week summer 2021</a>
                                            </h3>
                                            <p><a href="#">Patricia</a> / 28 September, 2021</p>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <section class="widget widget_tag_cloud">
                                <h3 class="widget-title">Tags</h3>

                                <div class="tagcloud">
                                    <a href="#">News</a>
                                    <a href="#">Business</a>
                                    <a href="#">Health</a>
                                    <a href="#">Politics</a>
                                    <a href="#">Magazine</a>
                                    <a href="#">Sport</a>
                                    <a href="#">Tech</a>
                                    <a href="#">Video</a>
                                    <a href="#">Global</a>
                                    <a href="#">Culture</a>
                                    <a href="#">Fashion</a>
                                </div>
                            </section>

                            <section class="widget widget_instagram">
                                <h3 class="widget-title">Instagram</h3>

                                <ul>
                                    <li>
                                        <div class="box">
                                            <img src="assets/img/latest-news/latest-news-1.jpg" alt="image">
                                            <i class="bx bxl-instagram"></i>
                                            <a href="#" target="_blank" class="link-btn"></a>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="box">
                                            <img src="assets/img/latest-news/latest-news-2.jpg" alt="image">
                                            <i class="bx bxl-instagram"></i>
                                            <a href="#" target="_blank" class="link-btn"></a>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="box">
                                            <img src="assets/img/latest-news/latest-news-3.jpg" alt="image">
                                            <i class="bx bxl-instagram"></i>
                                            <a href="#" target="_blank" class="link-btn"></a>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="box">
                                            <img src="assets/img/latest-news/latest-news-4.jpg" alt="image">
                                            <i class="bx bxl-instagram"></i>
                                            <a href="#" target="_blank" class="link-btn"></a>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="box">
                                            <img src="assets/img/latest-news/latest-news-5.jpg" alt="image">
                                            <i class="bx bxl-instagram"></i>
                                            <a href="#" target="_blank" class="link-btn"></a>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="box">
                                            <img src="assets/img/latest-news/latest-news-6.jpg" alt="image">
                                            <i class="bx bxl-instagram"></i>
                                            <a href="#" target="_blank" class="link-btn"></a>
                                        </div>
                                    </li>
                                </ul>
                            </section>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Default News Area -->
@endsection