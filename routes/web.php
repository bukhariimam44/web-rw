<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UmumController;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [UmumController::class, 'index'])->name('index');
Route::get('/kontak', [UmumController::class, 'kontak'])->name('kontak');
Route::get('/berita', [UmumController::class, 'berita'])->name('berita');
Route::get('/berita/{judul}', [UmumController::class, 'detail_berita'])->name('detail-berita');
Route::get('/tentang', [UmumController::class, 'tentang'])->name('tentang');
Route::get('/video', [UmumController::class, 'video'])->name('video');
Route::get('/galeri', [UmumController::class, 'galeri'])->name('galeri');
Route::get('/pengurus', [UmumController::class, 'pengurus'])->name('pengurus');
Route::get('/struktur-organisasi', [UmumController::class, 'struktur_organisasi'])->name('struktur-organisasi');

Route::get('/login', [LoginController::class, 'login'])->name('login');
Route::get('/daftar', [LoginController::class, 'daftar'])->name('daftar');